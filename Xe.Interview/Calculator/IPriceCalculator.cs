﻿using System;
using System.Collections.Generic;
using System.Text;
using Xe.Interview.Models;

namespace Xe.Interview.Calculator
{
    public interface IPriceCalculator
    {
        decimal CalculateProductTotal(decimal productPrice, int productCount, VolumePrice? coupon = null);
    }
}
