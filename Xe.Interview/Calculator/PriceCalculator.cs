﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using Xe.Interview.Models;

namespace Xe.Interview.Calculator
{
    public class PriceCalculator : IPriceCalculator
    {
        private readonly ILogger<PriceCalculator> _logger;

        public PriceCalculator(ILogger<PriceCalculator> logger)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public decimal CalculateProductTotal(decimal productPrice, int productCount, VolumePrice? coupon = null)
        {
            decimal productTotal = 0.0M;

            int fullPriceItemCount = productCount;

            if (coupon != null)
            {
                var quotient = productCount / coupon.Volume;

                if (quotient > 0)
                {
                    productTotal += quotient * coupon.Price;
                    fullPriceItemCount = productCount % coupon.Volume;
                }

            }

            // finally add the remaining products that did not receive any discount
            productTotal += fullPriceItemCount * productPrice;

            return productTotal;
        }
    }
}
