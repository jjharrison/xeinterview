﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace Xe.Interview.Models
{
    [ExcludeFromCodeCoverage]
    public class Product
    {
        public Product(string name, decimal price)
        {
            Name = name;
            Price = price;
        }
        
        public string Name { get; }

        public decimal Price { get; }
    }
}
