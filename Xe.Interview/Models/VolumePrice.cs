﻿    using System;
using System.Collections.Generic;
using System.Text;

namespace Xe.Interview.Models
{
    public class VolumePrice
    {
        public VolumePrice(int volumn, decimal price)
        {
            Volume = volumn;
            Price = price;
        }

        public int Volume { get; }
        public decimal Price { get; }
    }
}
