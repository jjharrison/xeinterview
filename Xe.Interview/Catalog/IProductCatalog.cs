﻿using System;
using System.Collections.Generic;
using System.Text;
using Xe.Interview.Models;

namespace Xe.Interview.Catalog
{
    public interface IProductCatalog
    {
        void SetProductPrice(Product product);

        decimal GetProductPrice(string productName);
    }
}
