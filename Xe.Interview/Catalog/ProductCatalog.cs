﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using Xe.Interview.Models;

namespace Xe.Interview.Catalog
{
    public class ProductCatalog : IProductCatalog
    {
        private readonly ILogger<ProductCatalog> _logger;
        private readonly Dictionary<string, decimal> _productCatalog = new Dictionary<string, decimal>();

        public ProductCatalog(ILogger<ProductCatalog> logger)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public void SetProductPrice(Product product)
        {
            _logger.LogDebug("Adding product {Name} for price {Price} to the product catalog", product.Name, product.Price);

            // Should only be called once during setup, but lets make it idempotent
            if (_productCatalog.ContainsKey(product.Name))
            {
                _productCatalog[product.Name] = product.Price;
            }
            else
            {
                _productCatalog.Add(product.Name, product.Price);
            }
        }

        public decimal GetProductPrice(string productName)
        {
            // Going to let this blow up on null reference by design
            return _productCatalog[productName];
        }
    }
}
