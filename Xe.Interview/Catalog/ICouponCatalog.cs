﻿using System;
using System.Collections.Generic;
using System.Text;
using Xe.Interview.Models;

namespace Xe.Interview.Catalog
{
    public interface ICouponCatalog
    {
        void SetProductCoupon(string productName, VolumePrice coupon);

        bool TryGetProductCoupon(string productName, out VolumePrice coupon);
    }
}
