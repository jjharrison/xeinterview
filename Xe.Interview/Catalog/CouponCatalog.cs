﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using Xe.Interview.Models;

namespace Xe.Interview.Catalog
{
    public class CouponCatalog : ICouponCatalog
    {
        private readonly ILogger<CouponCatalog> _logger;
        // Going with a naive approach assuming it'll only ever be a 1 to 1 product => discount mapping. But would easily extend this to cope with 1 to many
        private readonly Dictionary<string, VolumePrice> _productCoupons = new Dictionary<string, VolumePrice>();

        public CouponCatalog(ILogger<CouponCatalog> logger)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public void SetProductCoupon(string productName, VolumePrice coupon)
        {
            _logger.LogDebug("Setting price {Price} per {Count} items for product {ProductName}", coupon.Price, coupon.Volume, productName);

            if (_productCoupons.ContainsKey(productName))
            {
                _productCoupons[productName] = coupon;
            }
            else
            {
                _productCoupons.Add(productName, coupon);
            }
        }

        public bool TryGetProductCoupon(string productName, out VolumePrice coupon)
        {
            return _productCoupons.TryGetValue(productName, out coupon);
        }
    }
}
