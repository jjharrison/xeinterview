﻿using Microsoft.Extensions.Logging;
using System;
using Xe.Interview.Calculator;
using Xe.Interview.Cart;
using Xe.Interview.Catalog;
using Xe.Interview.Models;

namespace Xe.Interview
{
    public class PointOfSaleTerminal
    {
        private readonly ILogger<PointOfSaleTerminal> _logger;
        private readonly IProductCatalog _productCatalog;
        private readonly ICouponCatalog _couponCatalog;
        private readonly ICartService _cartService;
        private readonly IPriceCalculator _priceCalculator;

        public PointOfSaleTerminal(
            ILogger<PointOfSaleTerminal> logger,
            ICartService cartService,
            IProductCatalog productCatalog,
            ICouponCatalog couponCatalog,
            IPriceCalculator priceCalculator)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _cartService = cartService ?? throw new ArgumentNullException(nameof(cartService));
            _productCatalog = productCatalog ?? throw new ArgumentNullException(nameof(productCatalog));
            _couponCatalog = couponCatalog ?? throw new ArgumentNullException(nameof(couponCatalog));
            _priceCalculator = priceCalculator ?? throw new ArgumentNullException(nameof(priceCalculator));
        }

        public void SetPricing(Product product, VolumePrice? coupon = null)
        {
            _logger.LogDebug("Setting product {ProductName} price as {Price} with a discount price of {Pricing} per {Volumn} items", 
                product.Name,
                product.Price,
                coupon?.Price,
                coupon?.Volume
            );

            _productCatalog.SetProductPrice(product);

            if (coupon != null)
            {
                _couponCatalog.SetProductCoupon(product.Name, coupon);
            }
        }

        public void ScanProduct(Product product)
        {
            _cartService.AddProductToCart(product);
        }

        public decimal CalculateTotal()
        {
            decimal total = 0.0M;
            var cartItems = _cartService.GetCartItems();
            
            foreach (string productName in cartItems.Keys)
            {
                var productTotalPrice = _priceCalculator.CalculateProductTotal(
                    productPrice: _productCatalog.GetProductPrice(productName),
                    productCount: _cartService.GetCartItems()[productName],
                    coupon: _couponCatalog.TryGetProductCoupon(productName, out VolumePrice coupon) ? coupon : null
                );

                total += productTotalPrice;
            }

            return total;
        }
    }
}
