﻿using System;
using System.Collections.Generic;
using System.Text;
using Xe.Interview.Models;

namespace Xe.Interview
{
    public interface IPointOfSaleTerminal
    {
        void SetPricing();
        
        void ScanProduct(Product product);

        decimal CalculateTotal(); 
    }
}
