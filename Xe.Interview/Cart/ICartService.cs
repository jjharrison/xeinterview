﻿using System;
using System.Collections.Generic;
using System.Text;
using Xe.Interview.Models;

namespace Xe.Interview.Cart
{
    public interface ICartService
    {
        void AddProductToCart(Product product);

        public Dictionary<string, int> GetCartItems();
    }
}
