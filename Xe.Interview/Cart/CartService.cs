﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xe.Interview.Models;

namespace Xe.Interview.Cart
{
    public class CartService : ICartService
    {
        private readonly ILogger<CartService> _logger;
        private Dictionary<string, int> _cartItems = new Dictionary<string, int>();

        public CartService(ILogger<CartService> logger)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public Dictionary<string, int> GetCartItems() => _cartItems;

        public void AddProductToCart(Product product)
        {
            _logger.LogDebug("Adding product {ProductName} to cart", product.Name);

            if (!_cartItems.Keys.Contains(product.Name))
            {
                _cartItems.Add(product.Name, 0);
            }
            _cartItems[product.Name] += 1;
        }

    }
}
