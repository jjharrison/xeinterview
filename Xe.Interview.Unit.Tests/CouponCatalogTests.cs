﻿using System;
using System.Collections.Generic;
using System.Text;
using Xe.Interview.Catalog;
using Xe.Interview.Models;
using Xe.Interview.Unit.Tests.AutoMoq;
using Xunit;

namespace Xe.Interview.Unit.Tests
{
    public class CouponCatalogTests
    {
        [Theory, AutoMoqData]
        public void InsertOneProduct_CouponReturned(
            Product productOne,
            VolumePrice coupon,
            CouponCatalog sut)
        {
            sut.SetProductCoupon(productOne.Name, coupon);

            var hasCoupon = sut.TryGetProductCoupon(productOne.Name, out VolumePrice result);

            Assert.True(hasCoupon);
            Assert.Equal(coupon, result);
        }

        [Theory, AutoMoqData]
        public void InsertProductTwice_SecondCouponReturned(
           Product productOne,
           VolumePrice coupon,
           VolumePrice couponTwo,
           CouponCatalog sut)
        {
            sut.SetProductCoupon(productOne.Name, coupon);
            sut.SetProductCoupon(productOne.Name, couponTwo);

            var hasCoupon = sut.TryGetProductCoupon(productOne.Name, out VolumePrice result);

            Assert.True(hasCoupon);
            Assert.Equal(couponTwo, result);
        }

        [Theory, AutoMoqData]
        public void CouponNotFound(
           Product productOne,
           Product productTwo,
           VolumePrice coupon,
           CouponCatalog sut)
        {
            sut.SetProductCoupon(productOne.Name, coupon);

            var hasCoupon = sut.TryGetProductCoupon(productTwo.Name, out VolumePrice result);

            Assert.False(hasCoupon);
            Assert.Null(result);
        }
    }
}
