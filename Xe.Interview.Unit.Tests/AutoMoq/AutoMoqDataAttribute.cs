﻿using AutoFixture.Xunit2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Xe.Interview.Unit.Tests.AutoMoq
{
    public class AutoMoqDataAttribute : AutoDataAttribute
    {
        public AutoMoqDataAttribute() : base(FixtureFactory.Create) { }

    }
}
