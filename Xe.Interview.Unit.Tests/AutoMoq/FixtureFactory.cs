﻿using AutoFixture;
using AutoFixture.AutoMoq;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Xe.Interview.Calculator;
using Xe.Interview.Cart;
using Xe.Interview.Catalog;

namespace Xe.Interview.Unit.Tests.AutoMoq
{
    public class FixtureFactory
    {
        public static IFixture Create()
        {
            IFixture fixture = new Fixture();
            fixture.Customize(new AutoMoqCustomization());

            fixture.Register(() =>
            {
                var posTerminal = new PointOfSaleTerminal(
                    Mock.Of<ILogger<PointOfSaleTerminal>>(),
                    new CartService(Mock.Of<ILogger<CartService>>()),
                    new ProductCatalog(Mock.Of<ILogger<ProductCatalog>>()),
                    new CouponCatalog(Mock.Of<ILogger<CouponCatalog>>()),
                    new PriceCalculator(Mock.Of<ILogger<PriceCalculator>>())
                );

                return posTerminal;
            });
            return fixture;
        }
    }
}
