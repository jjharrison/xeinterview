using AutoFixture.Xunit2;
using System;
using Xe.Interview.Cart;
using Xe.Interview.Catalog;
using Xe.Interview.Models;
using Xe.Interview.Unit.Tests.AutoMoq;
using Xunit;

namespace Xe.Interview.Unit.Tests
{
    public class PointOfSaleTerminalTests
    {
        private Product _productA = new Product("A", 1.25M);
        private Product _productB = new Product("B", 4.25M);
        private Product _productC = new Product("C", 1.00M);
        private Product _productD = new Product("D", 0.75M);

        private void SetUpPrices(PointOfSaleTerminal sut)
        {
            sut.SetPricing(_productA, new VolumePrice(3, 3.0M));    // A $1.25 each, or 3 for $3.00
            sut.SetPricing(_productB, coupon: null);                // B $4.25
            sut.SetPricing(_productC, new VolumePrice(6, 5.0M));    // C $1.00 or $5 for a six-pack
            sut.SetPricing(_productD, coupon: null);                // D $0.75
        }

        [Theory, AutoMoqData]
        public void ABCDABA(
            [Frozen] PointOfSaleTerminal sut)
        {
            SetUpPrices(sut);
            sut.ScanProduct(_productA);
            sut.ScanProduct(_productB);
            sut.ScanProduct(_productC);
            sut.ScanProduct(_productD);
            sut.ScanProduct(_productA);
            sut.ScanProduct(_productB);
            sut.ScanProduct(_productA);

            var result = sut.CalculateTotal();

            Assert.Equal(13.25M, result);
        }

        [Theory, AutoMoqData]
        public void CCCCCCC([Frozen] PointOfSaleTerminal sut)
        {
            SetUpPrices(sut);
            sut.ScanProduct(_productC);
            sut.ScanProduct(_productC);
            sut.ScanProduct(_productC);
            sut.ScanProduct(_productC);
            sut.ScanProduct(_productC);
            sut.ScanProduct(_productC);
            sut.ScanProduct(_productC);

            var result = sut.CalculateTotal();

            Assert.Equal(6M, result);
        }
        
        [Theory, AutoMoqData]
        public void ABCD([Frozen] PointOfSaleTerminal sut)
        {
            SetUpPrices(sut);
            sut.ScanProduct(_productA);
            sut.ScanProduct(_productB);
            sut.ScanProduct(_productC);
            sut.ScanProduct(_productD);

            var result = sut.CalculateTotal();

            Assert.Equal(7.25M, result);
        }
    }
}
