﻿using System;
using System.Collections.Generic;
using System.Text;
using Xe.Interview.Calculator;
using Xe.Interview.Models;
using Xe.Interview.Unit.Tests.AutoMoq;
using Xunit;

namespace Xe.Interview.Unit.Tests
{
    public class PriceCalculatorTests
    {
        [Theory, AutoMoqData]
        public void DefaultPrice_NoCoupons(
            decimal productPrice,
            int productCount,
            PriceCalculator sut)
        {
            var total = sut.CalculateProductTotal(productPrice, productCount, coupon: null);

            Assert.Equal(productPrice * productCount, total);
        }

        [Theory, AutoMoqData]
        public void DefaultPrice_CouponAppliedZero(
            decimal productPrice,
            PriceCalculator sut)
        {
            int productCount = 5; 
            var discountPrice = productPrice - 1;
            var numDiscounted = 6;

            var coupon = new VolumePrice(numDiscounted, discountPrice);

            var total = sut.CalculateProductTotal(productPrice, productCount, coupon);

            Assert.Equal(productPrice * productCount, total);
        }

        [Theory, AutoMoqData]
        public void DefaultPrice_CouponAppliedOnce(
            decimal productPrice,
            PriceCalculator sut)
        {
            int productCount = 7;
            var discountPrice = productPrice - 1;
            var numDiscounted = 6;

            var coupon = new VolumePrice(numDiscounted, discountPrice);

            var total = sut.CalculateProductTotal(productPrice, productCount, coupon);

            var expectedTotal = (productPrice * 1) + discountPrice;

            Assert.Equal(expectedTotal, total);
        }

        [Theory, AutoMoqData]
        public void DefaultPrice_CouponAppliedTwice(
            decimal productPrice,
            PriceCalculator sut)
        {
            int productCount = 13;
            var discountPrice = productPrice - 1;
            var numDiscounted = 6;

            var coupon = new VolumePrice(numDiscounted, discountPrice);

            var total = sut.CalculateProductTotal(productPrice, productCount, coupon);

            var expectedTotal = (productPrice * 1) + (discountPrice * 2);

            Assert.Equal(expectedTotal, total);
        }
    }
}
