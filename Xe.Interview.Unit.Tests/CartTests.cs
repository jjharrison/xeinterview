﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xe.Interview.Cart;
using Xe.Interview.Models;
using Xe.Interview.Unit.Tests.AutoMoq;
using Xunit;

namespace Xe.Interview.Unit.Tests
{
    public class CartTests
    {

        [Theory, AutoMoqData]
        public void AddSingleProduct_Once_Success(
            Product productOne,
            CartService sut)
        {
            sut.AddProductToCart(productOne);

            var cartItems = sut.GetCartItems();

            Assert.Equal(1, cartItems?.Keys.Count);
            Assert.Equal(productOne.Name, cartItems?.Keys.First());
            Assert.Equal(1, cartItems[productOne.Name]);
        }

        [Theory, AutoMoqData]
        public void AddSingleProduct_Twice_Success(
            Product productOne,
            CartService sut)
        {
            sut.AddProductToCart(productOne);
            sut.AddProductToCart(productOne);

            var cartItems = sut.GetCartItems();

            Assert.Single(cartItems.Keys);
            Assert.Equal(productOne.Name, cartItems?.Keys.First());
            Assert.Equal(2, cartItems[productOne.Name]);
        }

        [Theory, AutoMoqData]
        public void AddTwoProducts_Success(
           Product productOne,
           Product productTwo,
           CartService sut)
        {
            sut.AddProductToCart(productOne);
            sut.AddProductToCart(productTwo);

            var cartItems = sut.GetCartItems();

            Assert.Equal(2, cartItems?.Keys.Count);

            Assert.Contains(productOne.Name, cartItems.Keys);
            Assert.Equal(1, cartItems[productOne.Name]);

            Assert.Contains(productTwo.Name, cartItems.Keys);
            Assert.Equal(1, cartItems[productTwo.Name]);
        }
    }
}
